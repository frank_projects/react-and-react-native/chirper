import React, { Component } from 'react';
import { connect } from 'react-redux';
import Tweet from './Tweet';
import NewTweet from "./NewTweet"

class TweetPage extends Component {

    render(){
        console.log("TweetPage, this.props", this.props);
        const { id, replies } = this.props;
         return (
            <div>
                <Tweet id ={id}/>
                <NewTweet id = {id}/>
                {replies.length !== 0 && <h3 className = 'center'>Replies </h3>}
                <ul>
                    {replies.map((replyId) => {
                            return(
                                <li key = {replyId}>
                                    <Tweet id ={replyId}/>
                                </li>)
                    })}
                </ul>
                    )

            </div>

        )

    }
}

function mapStateToProps ({authedUser, tweets, users}, props){
    /*
    * {
    *   dispatch: ...,
    *   match: {
    *       params: {
    *           id: "8xf0y6ziyjabvozdd253nd"
    *       }
    *   }
    * }
    * */
    const { id } = props.match.params;
    return {
        id,
        replies: tweets[id] // gets the replies for that particular tweet
            ? tweets[id].replies.sort((a,b) => tweets[b].timestamp - tweets[a].timestamp) // here we are sorting by time
            : []
    }
}

export default connect(mapStateToProps)(TweetPage);