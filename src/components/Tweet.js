import React, { Component } from 'react';
import { connect } from 'react-redux';
import {formatTweet, formatDate} from "../utils/helpers";
import TiArrowBackOutline from 'react-icons/lib/ti/arrow-back-outline'
import TiHeartOutline from 'react-icons/lib/ti/heart-outline'
import TiHeartFullOutline from 'react-icons/lib/ti/heart-full-outline'
import { handleToggleTweet} from "../actions/tweets";
import { Link, withRouter } from "react-router-dom"

class Tweet extends Component {

    handleLike = (e) => {
        e.preventDefault();
        const { dispatch, tweet, authedUser } = this.props;
        dispatch(
            handleToggleTweet(
                {
                    id: tweet.id,
                    hasLiked: tweet.hasLiked,
                    authedUser
                }
            )
        )

    };

    toParent = (event, parentId) => {
        event.preventDefault();
        this.props.history.push(`/tweet/${parentId}`)
    };

    render(){


        const { tweet } = this.props;
        if (tweet === null){
            return (
                <p>Sorry this Tweet does not exist.</p>
            )
        }

        const { name, avatar, timestamp, text, hasLiked, likes, replies, id, parent} = tweet;

        return (

            <Link
                to={`/tweet/${id}`} // when you click on the tweet itself then you are taken to that particular tweet view
                className = 'tweet'>
                <img
                  src={avatar}
                  alt={`Avatar of ${name}`}
                  className='avatar'
                />
                 <div className = 'tweet-info'>
                    <div>
                        <span> { name }</span>
                        <div>{formatDate(timestamp)}</div>
                        {
                            parent &&
                                (
                                 <button className = 'replying-to' onClick = {(e) => this.toParent(e, parent.id)}>
                                     Replying to @{parent.author}
                                 </button>
                                )
                        }
                        <p>{text}</p>
                    </div>
                    <div className = 'tweet-icons'>
                       < TiArrowBackOutline />
                        <span>{replies !== 0 && replies }</span>
                         <button
                             className = 'heart-button'
                             onClick={this.handleLike}>
                             {
                                 hasLiked === true
                                     ?<TiHeartFullOutline color="red"/>
                                     :<TiHeartOutline/>
                            }

                         </button>
                        <span>{likes !== 0 && likes}</span>
                    </div>
                </div>
            </Link>
        )
    }
}

function mapStateToProps({authedUser, users, tweets} , {id}){
    const tweet = tweets[id];
    const parentTweet = tweet ? tweets[tweet.replyingTo] : null; // if Tweet exists then get replyingTo otherwise just make it null
    return {
        authedUser,
        tweet: tweet // if tweet exists then return otherwise just return null
            ? formatTweet(tweet, users[tweet.author], authedUser, parentTweet)
            : null
    }

}

export default withRouter(connect(mapStateToProps)(Tweet)) // withRouter allows us to have access to the router