import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import {connect} from 'react-redux';
import {handleInitialData} from "../actions/shared";
import Dashboard from './Dashboard'
import LoadingBar from 'react-redux-loading';
import NewTweet from './NewTweet';
import TweetPage from './TweetPage';
import Nav from './Nav';



class App extends Component {

  componentDidMount(){
    /* When this component mounts get the data */
      this.props.dispatch(handleInitialData())
  }
  render() {
    return (
        <Router>
            <Fragment>

                  <LoadingBar/>

                  <Nav/>
                  {
                      this.props.loading === true
                        ? null // if it's loading then don't render anything
                        : <div>
                              <Route path ='/' exact component = {Dashboard}/>
                              <Route path = '/tweet/:id' component={TweetPage}/>
                              <Route path = '/new' component={NewTweet}/>
                          </div>
                  }
            </Fragment>
        </Router>
    )
  }
}

function mapStateToProps({authedUser}){
    /* We only want to load the dashboard if the data is found
    * This function returns a map of loading where it's value is True iff authedUser is null*/
    return {loading: authedUser === null}
}

export default connect(mapStateToProps)(App); // this is so we have access to dispath() inside componentDidMount()