import React, { Component } from 'react';
import {connect} from 'react-redux';
import Tweet from './Tweet'
class Dashboard extends  Component {
    render(){
        return (
            <div>
                <h3 className = 'center'> Your Timeline </h3>
                <ul className= 'dashboard-list'>
                    {
                        this.props.tweetIds.map((id) => (
                            <li key = {id}>
                                <Tweet id={id}/>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}

function mapStateToProps({ tweets }){
    /* Function maps particular data to the props*/
    return {
        tweetIds: Object.keys(tweets)
            .sort((a,b) => tweets[b].timestamp - tweets[a].timestamp) // we sort based on time
    }
}

export default connect(mapStateToProps)(Dashboard); // we pass in the function to connect which will do the mapping