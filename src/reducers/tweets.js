import {RECEIVE_TWEETS, TOGGLE_TWEET, ADD_TWEET } from "../actions/tweets";

export function tweets(state={}, action){
    switch (action.type){
        case RECEIVE_TWEETS:
            return {
                ...state, // this spread the previous tweets on the object so we don't mutate the original state
                ...action.tweets,
            };
        case TOGGLE_TWEET:
            return {
                ...state, // this spread the previous tweets on the object so we don't mutate the original state
                [action.id]: { // specifically the id of tweet
                    ...state[action.id], // spread the rest of the stuff
                    likes: action.hasLiked === true
                        ? state[action.id].likes.filter((uid) => uid !== action.authedUser) // if it's been liked remove
                        : state[action.id].likes.concat([action.authedUser]) // otherwise add the id
                }

            };
        case ADD_TWEET:
            /* Two things we need to do:
            * 1. Add the tweet to state.tweets
            * 2. Update the replies array of the tweet that is being replied to */
            const { tweet } = action;

            let replyingTo = {};
            // if replyingTo is not null then we want to concat the current tweet.id into the replies array
            if (tweet.replyingTo !== null){
                // we create a new object which basically grabs the chunk of data for the tweet which is being replied to
                replyingTo = {
                    [tweet.replyingTo]: { // we get the id of the tweet which we're replying to
                        ...state[tweet.replyingTo], // we spread everything from that particular tweet
                        replies: state[tweet.replyingTo].replies.concat([tweet.id]) // we modify our new replies array adding id
                    }
                }
            }
            return {
                ...state,
                [action.tweet.id]: action.tweet, // to add the tweet into the tweet slice of the state
                ...replyingTo,

            };

        default:
            return state
    }
}