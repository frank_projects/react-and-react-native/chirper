import {combineReducers} from 'redux';
import {authedUser} from './authUser'
import {users} from './users'
import {tweets} from './tweets'
import { loadingBarReducer } from 'react-redux-loading';
// this returns the all the reducers since createStore only takes a single reducer
export default combineReducers({
    authedUser: authedUser,
    users: users,
    tweets: tweets,
    loadingBar: loadingBarReducer, // this reducer will help keep track of whether data is being loaded
})